import pandas as pd
import numpy as np


def get_actual_item(df_data):
    
    #sort unique item and user id
    unique_item = set(df_data['item_id'].tolist())
    unique_user = set(df_data['user_id'].tolist())
    
    #empty dict for shown and unshown movies
    d_show = {}
    d_dontshow = {}
    
    #make {user: shown films} dict
    for i in unique_user:
        temp = {i : df_data[df_data['user_id'] == i]['item_id'].tolist()}
        d_show.update(temp)     
    
    #make {user: unshown films} dict
    for k,v in d_show.items():
        lst = list(unique_item).copy()
        for i in v:
            lst.remove(i)
        temp = {k : lst}
        d_dontshow.update(temp)
        
    return d_dontshow


def get_df(temp_dict):

    df = pd.DataFrame()
    
    #dict user-item --> DataFrame
    for k, v in temp_dict.items():
        temp = pd.DataFrame()
        temp['user_id'] = [k for i in v]
        temp['item_id'] = v
        df = df.append(temp)
        
    return df


def create_train_data(data, n_item):
    
    #group by user and count item for user
    grps = data.groupby('user_id').agg('count')['item_id']
    train_data = pd.DataFrame()
    
    #sort user by amount of shown movies
    for ind, v in zip(grps.index, grps):
        if v > n_item:
            temp = data[data['user_id'] == ind]
            train_data = train_data.append(temp)
            
    X = train_data[['user_id', 'item_id']]
    y = train_data['score']        
            
    return X, y