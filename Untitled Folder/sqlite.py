import sqlite3
import pandas as pd
import os
import sys


class SQLLITE:
    
    def __init__(self, db_name):
        self.db_name = db_name
        
    
    def __create_connection(self):
        
        self.connection = sqlite3.connect(self.db_name)
        
        
    def __close_connection(self):
        
        self.connection.close()


    def data_from_sql(query):
        
        self.__create_connection()
        cursor = self.connection.cursor()
        res = cursor.execute(query)
        data_from_db = res.fetchall()
        
        self.__close_connection()

        return data_from_db
