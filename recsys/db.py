import pandas as pd
from recommender_model import RecSysSVD
import recsys_helper as rh
from component import sqlite

#data read and rename
data = sqlite.data_from_sql('SELECT * FROM data')






"""

#create training dataset
X, y = rh.create_train_data(data, 150)

#add predict score column for dataset
mdl = RecSysSVD()
mdl.fit(X,y)
X['predicted_score'] = mdl.predict(X)

# рекомендация для непросмотренных item
temp_dict = rh.get_actual_item(data)
df = rh.get_df(temp_dict)
df['predicted_score'] = mdl.predict(df)
df.set_index('user_id', inplace=True)

def top(user_id, count):
    
    top = df.loc[user_id].sort_values(by=['predicted_score'])[-count:]
    top_item_id = list(top['item_id'])
    temp = []

    for i in top_item_id:
        filter_id = data['item_id'] == i
        top_1 = set(data.loc[filter_id]['title'])
        for i in top_1:
            temp.append(i)

    for i in temp:
        print(i)
        
top(10, 15)

"""