import sqlite3
import pandas as pd


def data_from_sql(query):
    
    connection = sqlite3.connect('testdb.db')
    cursor = connection.cursor()
    res = cursor.execute(query)
    data_from_db = res.fetchall()
    connection.close()
    
    return data_from_db


data = data_from_sql('SELECT * FROM data')

#create DataFrame from db
df = pd.DataFrame(data)
df = df.rename({0:'index', 1:'user_id', 2:'item_id', 3:'score', 4:'title'}, axis=1)
df = df.drop('index', axis=1)

