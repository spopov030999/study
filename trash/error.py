import pandas as pd

def bank(s, y, p):
    
    try:
        y = int(y)
        s = int(s)
        p = int(p)
        for i in range(y):
            s += s*(p/100)
        return s
    except:
        return 'Error'
    
    
def bank2(s, y, p = 10):
    
    y = int(y)
    s = int(s)
    p = int(p)
    for i in range(y):
        s += s*(p/100)
    return s

    
data = pd.read_csv('vklad.csv')

#first
data['result'] = [bank(i, j, k) for i,j,k
    in zip(data['value'], data['year'], data['percent'])]

#second
temp = []

for i,j,k in zip(data['value'], data['year'], data['percent']):
    try:
        temp.append(bank2(i, j))
    except:
        temp.append('Error')
        
data['result2'] = temp

data.to_csv('error_result.csv', index = None)
