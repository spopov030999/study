import math
import numpy as np

def my_skewness(lst):
    
    g = math.sqrt(np.var(lst))
    m = np.mean(lst)
    s = 0
    l = len(lst)
    for i in lst:
        s += math.pow((i-m), 3)
    skew = s/l/math.pow(g, 3)
    return skew


def my_kurtosis(lst):
    
    g = math.sqrt(np.var(lst))
    m = np.mean(lst)
    s = 0
    l = len(lst)
    for i in lst:
        s += math.pow((i-m), 4)
    kurt = s/l/math.pow(g, 4) - 3
    return kurt