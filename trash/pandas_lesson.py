import pandas as pd

k = 3

def filt(input_filename, output_filename, k):
    temp = pd.read_csv(input_filename, header = None)
    output_lst = []
    
    for row in range(len(temp)):
        lst = [temp[i][row] for i in range(temp.shape[1])]
        temp_lst = []
        
        for i in range(len(lst)-(k-1)):
            temp_lst.append(sum(lst[i+j] for j in range(k)) / k)
        
        output_lst.append(temp_lst)
    output_lst = pd.DataFrame(output_lst)
    output_lst.to_csv(output_filename)
            
filt('temp.csv', 'results.csv', k)
